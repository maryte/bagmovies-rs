import sys
import xlrd
import numpy as np
import xlwt
import collaborative_filtering
import recomendations

def main(): 

	if len (sys.argv) != 4:
		print "Usage: <user_id> <input file> <output_file>"
		sys.exit (1)
		
	user_id = int(sys.argv[1])
	input_file = sys.argv[2]
	output_file = sys.argv[3]
		
	res_ = collaborative_filtering.collaborative_rec(user_id)
	res = []
	res = recomendations.associative_rec(user_id, input_file, output_file)
	
	set_res = []
	set_res = set(res)
	
	out = [val for val in res_ if val in set_res]

	

if __name__ == "__main__":
    main()
import sys
import xlrd
import numpy as np
import xlwt
from recommendation_data import ratings,movies

def associative_rec(user_id, input_file, output_file):
	# print "Data acquisition from ", input_file
	book = xlrd.open_workbook(input_file)
	history_sh = book.sheet_by_index(0)
	preference_mx_sh = book.sheet_by_index(2)

	#get user history
	user_history = history_sh.row_values(int(user_id))
	
	#get preference matrix
	preference_mx=[]
	for row in range (preference_mx_sh.nrows):
		_row = []
		for col in range (preference_mx_sh.ncols):
			_row.append(preference_mx_sh.cell_value(row,col))
		preference_mx.append(_row)

	preference_mx = np.array(preference_mx)	
	
	# print preference_mx
		
	#compute recommendation list
	recommendations = np.dot(user_history,preference_mx)
	
	# print "user_history: "
	# for i, value in enumerate(user_history):
		# if(value != 0):
			# print i
	
	# print "recommendations list before cleaning: "
	# for i, value in enumerate(recommendations):
		# if (value != 0):
			# print i
	
	# print "recommendations list after cleaning: "
	
	recommendations=recommendations.tolist()
	for i, value in enumerate(user_history):
		if (value != 0) :
			#print "value ", value, " removed from recc"
			recommendations[i]=0
	
	wb = xlwt.Workbook()
	ws = wb.add_sheet('recommendations')

	rec_movies=[]
	for i, value in enumerate(recommendations):
		ws.write(i, 0, value)
		if (value != 0):
			rec_movies.append(i)
			
	for i in rec_movies:
		print movies[i]

	wb.save(output_file)
	return rec_movies


def main(): 

	if len (sys.argv) != 4:
		print "Usage: <user_id> <input file> <output_file>"
		sys.exit (1)
		
	user_id = sys.argv[1]
	input_file = sys.argv[2]
	output_file = sys.argv[3]
	
	res =  associative_rec(user_id, input_file, output_file)

if __name__ == "__main__":
    main()
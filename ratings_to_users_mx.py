#!/usr/bin/env python
# Implementation of collaborative filtering recommendation engine

from recommendation_data import ratings,movies
from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt


def pearson_correlation(person1,person2):

	# To get both rated items
	both_rated = {}
	for item in ratings[person1]:
		if item in ratings[person2]:
			both_rated[item] = 1

	number_of_ratings = len(both_rated)		
	
	# Checking for number of ratings in common
	if number_of_ratings == 0:
		return 0

	# Add up all the preferences of each user
	person1_preferences_sum = sum([ratings[person1][item] for item in both_rated])
	person2_preferences_sum = sum([ratings[person2][item] for item in both_rated])

	# Sum up the squares of preferences of each user
	person1_square_preferences_sum = sum([pow(ratings[person1][item],2) for item in both_rated])
	person2_square_preferences_sum = sum([pow(ratings[person2][item],2) for item in both_rated])

	# Sum up the product value of both preferences for each item
	product_sum_of_both_users = sum([ratings[person1][item] * ratings[person2][item] for item in both_rated])

	# Calculate the pearson score
	numerator_value = product_sum_of_both_users - (person1_preferences_sum*person2_preferences_sum/number_of_ratings)
	denominator_value = sqrt((person1_square_preferences_sum - pow(person1_preferences_sum,2)/number_of_ratings) * (person2_square_preferences_sum -pow(person2_preferences_sum,2)/number_of_ratings))
	if denominator_value == 0:
		return 0
	else:
		r = numerator_value/denominator_value
		return r 
		
def main():

	if len (sys.argv) != 2:
		print "Usage: <output file>"
		sys.exit (1)
		
	output_file = sys.argv[1]
	
	users_mx = [[0 for m in range(len(ratings))] for u in range(len(ratings))] 
	
	for user in range(len(ratings)):
		for other in range(len(ratings)):
			users_mx[user][other] = pearson_correlation(user,other)
	
	# print users_mx
	
	wb = xlwt.Workbook()
	ws = wb.add_sheet('users_mx')

	for row, array in enumerate(users_mx):
		for col, value in enumerate(array):
			ws.write(row, col, value)

	wb.save(output_file)
	
	print '#new users_mx ', len(ratings), ' x', len(ratings), ' created'
	 

if __name__ == "__main__":
    main()
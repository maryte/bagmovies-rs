from recommendation_data import ratings,movies
from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt

def main(): 

	if len (sys.argv) != 2:
		print "Usage: <output file>"
		sys.exit (1)
		
	output_file = sys.argv[1]
	
	preference_mx = [[0 for m in range(len(movies))] for u in range(len(ratings))] 
	
	#write preference matrix
	for user in ratings:
		for movie in ratings[user]:
			preference_mx[user][movie] = ratings[user][movie]
		
	# print preference_mx

	wb = xlwt.Workbook()
	ws = wb.add_sheet('preference_mx')

	for row, array in enumerate(preference_mx):
		for col, value in enumerate(array):
			ws.write(row, col, value)

	# wb.save('preference_mx.xls')

	transpose_preference_mx = list(map(list, zip(*preference_mx)))

	# print transpose_preference_mx

	ws = wb.add_sheet('transpose_preference_mx')

	for row, array in enumerate(transpose_preference_mx):
		for col, value in enumerate(array):
			ws.write(row, col, value)

	wb.save('outputs/preference_mxs.xls')

	preference_mx = np.asarray(preference_mx)
	transpose_preference_mx = np.asarray(transpose_preference_mx)

	similarity_mx = np.dot(transpose_preference_mx,preference_mx)

	ws = wb.add_sheet('similarity_mx')

	for row, array in enumerate(similarity_mx):
		for col, value in enumerate(array):
			ws.write(row, col, value)

	wb.save(output_file)
	
	print '#new movie_mx ', len(movies), ' x', len(movies), ' created'

if __name__ == "__main__":
    main()
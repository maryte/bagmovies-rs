\select@language {english}
\contentsline {section}{\numberline {1}Executive Summary}{4}
\contentsline {section}{\numberline {2}User's Manual}{5}
\contentsline {subsection}{\numberline {2.1}Description of the different functionalities of the IP System}{5}
\contentsline {subsection}{\numberline {2.2}Examples of use}{6}
\contentsline {subsection}{\numberline {2.3}Interactions of the system}{7}
\contentsline {section}{\numberline {3}Technical/System Manual}{7}
\contentsline {subsection}{\numberline {3.1}Functional Architecture of the System}{7}
\contentsline {section}{\numberline {4}Solution Design}{8}
\contentsline {subsection}{\numberline {4.1}Top-rated approach}{8}
\contentsline {subsection}{\numberline {4.2}Collaborative-filtering approach}{8}
\contentsline {subsection}{\numberline {4.3}Social (Associative) approach}{9}
\contentsline {subsection}{\numberline {4.4}The resulting hybrid approach}{9}
\contentsline {subsection}{\numberline {4.5}Further extension}{10}
\contentsline {section}{\numberline {5}Economic Cost Analysis}{11}
\contentsline {section}{\numberline {6}Sustainability/Environmental Analysis}{12}
\contentsline {section}{\numberline {7}Final IS Project Scheduling}{12}
\contentsline {section}{\numberline {8}The IS Project Time Sheet}{12}

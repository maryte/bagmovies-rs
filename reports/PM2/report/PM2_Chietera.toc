\select@language {english}
\contentsline {section}{\numberline {1}Why BAGmovies?}{4}
\contentsline {section}{\numberline {2}Requirement Analysis}{4}
\contentsline {section}{\numberline {3}Functional Architecture of the System}{5}
\contentsline {section}{\numberline {4}Strategy Outline}{6}
\contentsline {subsection}{\numberline {4.1}Top-rated Approach}{6}
\contentsline {subsection}{\numberline {4.2}Social Approach}{6}
\contentsline {subsection}{\numberline {4.3}Associative Approach}{7}
\contentsline {subsection}{\numberline {4.4}Tag-based Approach}{7}
\contentsline {section}{\numberline {5}Initial Project Scheduling}{8}
\contentsline {section}{\numberline {6}Software Prototype}{8}

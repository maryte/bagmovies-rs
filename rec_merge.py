import sys
import xlrd
import numpy as np
import xlwt
import rt_collaborative_filtering
import rt_associative_filtering
from recommendation_data import ratings,movies

def main(): 

	if len (sys.argv) != 5:
		print "Usage: <user_id> <users_mx> <movies_mx> <output_file>"
		sys.exit (1)
		
	user_id = int(sys.argv[1])
	users_mx = sys.argv[2]
	movies_mx = sys.argv[3]
	output_file = sys.argv[4]
	
	print "\nThis user already rated: "
	for i in ratings[user_id]:
		print "\t", movies[i]
	
		
	res_ = rt_collaborative_filtering.collaborative_rec(user_id, users_mx)
	res = []
	res = rt_associative_filtering.associative_rec(user_id, movies_mx, output_file)
	
	set_res = []
	set_res = set(res)
	
	#out = [val for val in res_ if val in set_res]
	out = list(set(res_).union(res))
	print "\nCollaborative filtering result: "
	for i in res_:
		print "\t", movies[i]
	print "Associative filtering result: "
	for i in res:
		print "\t", movies[i]
	print "\nFinal reault: "
	for i in out:
		print "\t", movies[i]

	

if __name__ == "__main__":
    main()
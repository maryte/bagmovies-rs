#!/usr/bin/env python
from recommendation_data import ratings,movies
from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt
from recommendation_data import ratings,movies

def collaborative_rec(person,input_file):

	book = xlrd.open_workbook(input_file)
	person_sh = book.sheet_by_index(0)

	#get user list of person indexes
	user_sim = person_sh.row_values(int(person))
	
	# Gets recommendations for a person by using a weighted average of every other user's rankings
	totals = {}
	simSums = {}
	rankings_list =[]
	
	for other in range(len(user_sim)):
		# don't compare me to myself
		if other == person:
			continue
		#print user_sim[other]
		# ignore scores of zero or lower
		if user_sim[other] <=0: 
			continue
		#for each item rated by a similar user
		for item in ratings[other]:
			# print "similar user: ", other
			# only score movies the user has not seen seen yet
			if item not in ratings[person]:
				# Similarity * score
				# print "movie: ", item
				totals.setdefault(item,0)
				totals[item] += ratings[other][item]* user_sim[other]
				# sum of similarities
				simSums.setdefault(item,0)
				simSums[item]+= user_sim[other]

	# Create the normalized list
	rankings = [(total/simSums[item],item) for item,total in totals.items()]
	rankings.sort()
	rankings.reverse()
	# print rankings
	# returns the recommended items
	recommendations_list = [recommend_item for score,recommend_item in rankings]
	return recommendations_list
		
def main():

	if len (sys.argv) != 3:
		print "Usage: <user_id> <input_file>"
		sys.exit (1)
		
	user_id = int(sys.argv[1])
	input_file = sys.argv[2]
	
	res = collaborative_rec(user_id,input_file)
	
	for i in res:
		print movies[i]
	 

if __name__ == "__main__":
    main()